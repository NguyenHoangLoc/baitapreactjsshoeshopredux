import React, { Component } from "react";
import { connect } from "react-redux";

class DetailShoe extends Component {
  render() {
    let { name, id, price, description, image } = this.props.detail;
    console.log(" this.props: ", this.props);
    return (
      <div className="mt-5  p-5 alert-secondary">
        <h1>Details Shoe</h1>
        <div className="row text-left">
          <img src={image} alt="" className="col-3" />
          <div className="col-9">
            <p>ID : {id} </p>
            <h5>NAME :{name} </h5>
            <p>DESC :{description} </p>
            <p>PRICE :{price} </p>
          </div>
        </div>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    detail: state.shoeReducer.detail,
  };
};
export default connect(mapStateToProps)(DetailShoe);
