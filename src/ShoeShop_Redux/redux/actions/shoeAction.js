import { SHOW_DETAIL } from "../constants/shoeConstant";

export const changDetailAction = (value) => {
  return {
    type: SHOW_DETAIL,
    payload: value,
  };
};
