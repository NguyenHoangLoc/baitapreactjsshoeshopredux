export const CHANGE_QUANTITY = "CHANGE_QUANTITY";

export const ADD_TO_CART = "ADD_TO_CART";

export const SHOW_DETAIL = "SHOW_DETAIL";
