import logo from "./logo.svg";
import "./App.css";
import ShoeShop_Redux from "./ShoeShop_Redux/ShoeShop_Redux";

function App() {
  return (
    <div className="App">
      <ShoeShop_Redux />
    </div>
  );
}

export default App;
